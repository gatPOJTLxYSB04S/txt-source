所謂的探索者這個職業。

迷宮的潛入、入手寶物、打倒魔物、取得魔石。
或是踏破迷宮。而那都是想成為英雄志願者的夢想。那是無法習得魔法的人，殘存的最後希望。

更何況這裡是不會死的迷宮。即使死的根渣渣一樣，也一樣會甦醒過來。但會失去身上所有財產。

所以站在分辨的立場。死掉的都是笨蛋。
能明白自己的極限到哪裡，這才是聰明人的做法。
在那裡沒有驕傲、意氣用事。也沒有覺悟。
迷宮都市的探索者就是這樣的東西。

但是，有時才會出現的。
自己無能為力的階層、無法觸及的世界，卻輕易走著的強者。
而能不能踏破迷宮才不是問題。只能滿足於自己腳下渺小的世界嗎、能明白嗎，那樣的痛苦。
所以用力的吠叫吧。弱小的狗。

況且對方，看起來比自己年輕、矮小、還要弱。

打開門進來的是貓獸人的少女。
是在公會中的熟面孔。中堅實力隊伍的偵查奴隷，而隊伍笨蛋似的全滅了，在這是已經都知道了的事。

這次來的事，大概是重新購買吧。裝備看起來也變新了，受到的待遇肯定是變好了吧。

「瑪露，我很擔心你呦。在同一天出發又回來」
「嗯、我也是這麼想的，但莉雅醬⋯」

看見了之後跟著進來的人，所有探索者的目光都被吸引過去。

───

還帶著有點稚嫩的氣氛、亮麗的美貌。隨風波動般的黑髮、還帶著細微的反光。
黑色統一的服裝，極其簡樸的皮鎧。腰上掛著２把刀。

「怎樣？有收獲嗎？」

貓獸人繼續和接待的人員對話著。

「嗯、很厲害呦。莉雅直接把牆壁破壊掉了喔」

那是初心者所犯的錯誤啊。但是卻生還了，大概是運氣好吧。

「地獄獵犬直接用空手打，跟寵物一樣呢」

實在無法相信。

「打倒了惡魔後，就先回來了～」

那種事情實在是不可能。

「喂喂，別到處宣揚啊，要適可而止喔」

在開心說著的瑪露身後，冒出了男人的探索者聲音。

以前的話，在這時就沉默了吧。

「我說的是真的！有好好的採集魔結晶喔！」

正好在這時，卡洛斯把惡魔的魔結晶放置在櫃台的檢定裝置上。

「啊、這個是牛頭人、格雷姆、骸骨劍士、地獄獵犬⋯」

粗糙的魔石也被放在上面。魔石的純度和正常的一樣，但數量也太多了吧。

「那、那個、你們是怎麼採集魔石的呢？」

鑑定的人表情抽搐的問道。
普通情況下，就算中堅實力的隊伍長期潛入，採集的量也只有十分之一而已。

「我們家的小姐把牆壁破壊時，把聚集過來的魔物依序擊殺而已」

卡洛斯用著疲勞的聲音解釋著。如果潛入迷宮，遭遇到一樣的事情的話，會覺得厭煩也不是沒有道理。

「⋯我明白了。還有其他在迷宮中得到的物品嗎？」
「主要是毛皮和牙⋯總之全部拿出來吧」

魔物用過的武器和防具，質量好的東西也全都拿出來了。其他的像是昆虫的甲殻、刃的部分、幾個不必要的格雷姆核心。而魔法藥之類的，大概用不到吧，也全都拿了出來。

「這是⋯因為沒辦法馬上鑑定出來⋯明天有時間嗎？」

這是當然的吧。卡洛斯點著頭。

那個時候視線全都聚集在櫃台上，探索者們也在旁邊竊竊私語。

傳言不斷的流傳著。偶爾出現的，輕易的跨過那道牆的人。不、那道牆想都沒想過吧。那是傳說般的人。
但是沒人想要率直的去承認，這是當然的。

「喂喂。到底使用了什麼方法啊？也告訴我們吧」

全身穿著盔甲，有著粗曠氣氛的戰士。慣例般的浮現出討好的笑容、俯視著瑪露。
瑪露想進一步說話時，肩膀被莉雅溫柔的搭住。

「沒什麼。就直接從正面擊潰了而已」

眼前的男人，在瑪露面前也稱不上敵人吧。而瑪露的等級現在已經到達了４０等。
短短的三天內，等級多了整整一倍。

「是小姐用的吧」

在說的同時伸出了手，緊握住莉雅的手。

「什、嗚哇！」

有如哥布林一樣的握力緊抓著莉雅。在適當的時機手被拉開了，用怪物般的眼神看著。

「住手吧。那些人的等級，連最低的都有４０等」

在後方像魔法師的男人小聲的說著。使用了鑑定的魔法了吧。

「奧加有５５等。那個女孩不知道⋯」
「不知道？有看破耐性？」
「也有可能是魔法。不管怎樣，我們不是對手」

畏懼的感情涌現。滿足的莉雅抱著胳膊挺起胸。

「好了莉雅、走吧」

為了避免造成這以上的麻煩，在露露的催促下，一行人走出了公會。

───

回到了市長的宅邸，克勞斯外出了，由執事一行人照顧。

在晚餐時協商探索必要的物資，克勞斯感到非常驚訝，久違好好的洗了個澡和安穩的睡眠。

莉雅一邊把瑪露當成抱枕一邊想著。
在迷宮中感受到了，那個氣息。

迷宮的主人主要被認為是魔族。
但是，也沒有殺人類。人族和魔族沒有敵對嗎。
原本魔族，是居住在絶對凍土的對面魔族領域的亞人總稱，以前奧加也是魔族。據說現在仍有屬於魔族的奧加。

那麼，儘管是魔族，沒有和人類有好的存在嗎。到不如說，可以被稱之為魔族？

而迷宮從也被認為踏破無數次，但迷宮主沒有被消滅。證據就是迷宮還在這邊。為甚麼沒有被消滅呢，是認為沒有必要消滅嗎？

還沒想出結論，莉雅就睡著了。

───

接下來的一天，準備休養和花費了。

首先卡洛斯為了找盾和劍在武器・防具店巡游，同時薩基和露露湊齊魔法藥。
薩基最後買了一把杖。可以加速水魔法和土魔法的啟動速度。攻擊力不論，稍為解決了防御的困擾。

「如果第１０層的BOSS是竜的話，防御的魔法是絶對必要的」

卡洛斯為了魔法的抵抗力買了秘銀盾。雖然材料本身也是如此，但是也被賦予了魔法，單純的質量攻擊以外也很有用。
不愧是一直使用鎧甲的人。所以薩基一邊看魔導書一邊用硬度強化、靱性強化、輕量化的賦予魔法。

「好厲害啊、看著那種書還能邊施展魔法」

露露真心感到佩服，那也是天賦的效果。

「露露是混血精靈，沒辦法用精靈魔法嗎？」
「嗚⋯那個是和魔法不太一樣的特殊技能所以⋯」

在地上畫著圈圈的露露說著。稍微的自卑感。

───

吃完晚餐後在同一間房間集合了，互相討論了當天調查的情報。

各層的守護者關係已經從奧加王那裡聽說了。
問題是去到那裡的路。現役的探索者幾乎沒有到第８層的，而已經引退了的探索者，也不知詳細的資訊。但是，隨著階層往下，敵人越強、迷宮越窄這是可以確定的事。

「第６層的鐵格雷姆、第７層的死靈騎士、第８層的獨眼魔、第９層的九頭蛇。至今為止經過的階層來看，BOSS相關的魔物也會出現吧」

瑪露舉手發問。

「那個、其他魔物大概明白，但獨眼魔沒聽過」

確實是瑪露的同伴那聽說的，說明一下吧。獨眼魔是用魔法製造出來的模擬生物，相當於小型魔物。
大致的說明了一下，薩基有點頭緒、但那之中細微的不同不太明白。

「抹殺世上的幼兒性愛者、聽說是古代帝國時代製作的⋯」
「第一次聽到呢」

露露在當盧法斯的助手時偶然的問到那個，得到了詳細的說明。
為了隔天的攻略，一行人便提早睡了。

當然瑪露忠實的執行著抱枕的任務。