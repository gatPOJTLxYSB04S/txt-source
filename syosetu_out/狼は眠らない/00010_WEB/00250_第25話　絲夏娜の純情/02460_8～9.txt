８

「真是辛苦您了」

絲夏娜小姐交出了柔軟的毛巾，如此說道。

雷肯雖然在解決了石匠和木匠的對峙後就回了領主館，但問題接連發生，席不暇暖，到處奔波。
到打架的現場，把雙方的當事人毆倒。
到事故現場，讓有過失的一方約定會賠償。
有人受傷，就施予〈回復〉。
也順便給領主施〈回復〉。
有動不了的貨車就抬起來。

總之做了能做的事。
沒對不擅長的領域出手。
舉例來說，沒在竊盜事件上出動。
也沒有干涉資材的安排，作業的程序等問題。

自己就算去了那種地方也沒辦法讓事態有所進展。
把時間花在辦不到的事情上也沒有用。

「啊啊」

擦了汗，把架子裡的酒拿出來倒在銀杯裡，一口飲盡。
好酒。

「結果，沒能吃到午餐呢」

絲夏娜雖然準備了午飯，但發生了流血事件便趕了過去，之後也沒有坐下來吃飯的時間。

「啊啊。吃了帶在身上的肉乾」
「阿呀」

對絲夏娜來說，邊走邊吃這種沒禮貌的行為，大概連想像都想像不到吧。

「那麼，既然力通也回來了，那我也回家去了」
「誒？不，那個。晚餐的準備」
「回家就會有人幫忙做晚飯。不用操心」

對還是想挽留的絲夏娜告別後，雷肯就離開了。

想早點回去，配著艾達的料理慢慢喝酒。
最近，料理的種類增加了許多。艾達在家庭方面的各種能力，順利地提升著。


９

隔天從早上到午後都在希拉那邊練習〈障壁〉。

持續集中在感覺做不到，又不知道要怎麼做到的事上，是非常疲憊的作業。
精疲力盡的雷肯，拜訪了諾瑪的施療所。
諾瑪正帶著艾達出診，眺望著庭院一陣子後就回來了。

「你說什麼。領主的令嬡，親自幫你泡了茶端上來嗎？」
「啊啊。是感謝的證明吧」
「雷肯。說是要讓你住的那間房間旁邊，有女性的房間嗎」
「這麼說來，隔壁的房間有化妝台。裡頭也有傭人的房間」
「雷肯」
「嗯？」
「知道訪妻婚嗎」
「不知道」
「是丈夫拜訪妻子的住處的結婚形式」
「什麼阿，那個？結婚的話就會一起住吧」
「庶民的話就是。但如果是貴族，也有不能那樣的場合」
「為什麼明明是夫妻卻不能一起住？」
「會是訪妻婚，就是因為有某種理由，讓妻子不能進丈夫的家」
「雖然不太懂，但這場合讓丈夫進妻子的家不就好了嗎？」
「能那樣的話，當然會那樣做吧。就舉幾個不能那樣的場合吧。舉例來說，某個国家的王族，定期拜訪別国的某個貴族，然後對那貴族的女兒一見鍾情的場合。雖然嫁到他国就好，但對本人和雙方的家族來說，不一定會幸福。在這種場合，只有去那貴族那裡時才會是結婚的狀態」
「這算是結婚嗎？」
「算結婚。如果生了孩子，那貴族的家就能抱有流著他国王族的血的孩子，這在政治上有非常重大的意義」
「完全搞不懂」
「嗯。很難懂吧。雖然剛剛講的是夫與妻所屬於不同的国家的場合，但在同個国家裡也會發生類似的事。同個城鎮也會有這種事」
「簡單來說就是，丈夫的身分比妻子的高太多，兩家的立場差太多，所以結了婚會變得不幸。因此丈夫就會拜訪妻子的家是吧」
「沒錯，就是那樣！什麼嘛。不是有好好理解嗎」
「不。沒自信有理解到」
「哈哈。那麼，來想想別的場合吧。妻子是身分高的貴族令嬡。丈夫是身分低又沒什麼財產的貴族的次男或三男，雖然是有前途的年輕人，但現在還沒有地位和財產」
「什麼？呼嗯」
「身分高的貴族，期待年輕人的將來，允許結婚。但是女兒要留在自己家，會建造別棟之類的，採取年輕人拜訪那裡的形式」
「讓年輕人也一起住不就好了嗎？」
「嘛，事實上，過的生活就等於在居住吧。但就算是這樣的場合，在形式上也是訪妻婚喔」
「搞不懂意義」
「年輕人如果發跡了，得到了身分地位，身分高的貴族，說不定就會正式將年輕人作為女兒的丈夫迎到自己家。又或者，說不定會成立別的家，把女兒嫁過去」
「沒有發跡的話，會怎樣？」
「身分高的貴族，會在某個時間點放棄年輕人，把女兒嫁到別處吧。到建立關係能有政治上的價值的家去」
「⋯⋯」
「很困難嗎？」
「也就是說，訪妻婚不是正式的結婚對吧？在之後，能說那是結婚，也能說那不是結婚。就是像貴族擅長的花言巧語的奇妙的結婚對吧？」
「真有趣的理解呢。不過，那有正確說中訪妻婚的一種性格喔。也是有那種訪妻婚。也有會被視為一種正式的結婚型態的訪妻婚就是了」
「貴族這東西很複雜，又再次了解到了」
「這樣阿。那麼，已經理解了吧？」
「理解什麼？」
「你跟絲夏娜小姐的訪妻婚正要成立了喔」